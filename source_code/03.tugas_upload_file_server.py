import socket

HOST = socket.gethostname()
PORT = 48088
BUFFSIZE = 1024

soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
soc.bind((HOST, PORT))
soc.listen(1)
print('Waiting for client...')

conn, addr = soc.accept()
print('Connected to', addr)
f = open('hasil_upload.txt', 'wb')  # Open in binary
while 1:
    data = conn.recv(BUFFSIZE)
    while data:
        f.write(data)
        data = conn.recv(BUFFSIZE)
    f.close()
    print("Upload success.")
    break

conn.close()
soc.close()
