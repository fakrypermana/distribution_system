import socket

HOST = '192.168.1.17'  # The server's hostname or IP addres
PORT = 55858  # The port used by the server
BUFFSIZE = 1024

soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
soc.connect((HOST, PORT))

while True:
    MSG = input('Enter for message: ')
    soc.sendall(bytes(MSG.encode()))
    if MSG == '0':
        break
    data = soc.recv(BUFFSIZE)
    print('\tmessage received:', data.decode())
soc.close()
