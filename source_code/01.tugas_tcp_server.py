import socket

HOST = socket.gethostname()  # Server's IP
PORT = 55858  # Port to listen on (non-privileged ports are > 1023)
BUFFSIZE = 1024

soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
soc.bind((HOST, PORT))
soc.listen(1)

print('TCP Connection Established\nWaiting for client...')
conn, addr = soc.accept()
print('Connected by', addr)
while True:
    data = conn.recv(BUFFSIZE)
    if data:
        if data.decode() == '0':
            conn.close()
            print('TCP Connection is closed')
            break
        print('Message from ', addr, ':', data.decode())
        conn.sendall(data)
