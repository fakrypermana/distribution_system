#!usr/bin/python

import socket

sock = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)      # For UDP

udp_host = "192.168.1.17"	# Host IP
udp_port = 12345			        # specified port to connect
BUFFSIZE = 1024


print("UDP target IP:", udp_host)
print("UDP target Port:", udp_port)

while True:
    msg = input("Enter Message: ")
    sock.sendto(msg.encode('utf-8'), (udp_host, udp_port))
    if msg == '0':
        break
    data = sock.recv(BUFFSIZE)
    print('\tmessage received:', data.decode())

sock.close()